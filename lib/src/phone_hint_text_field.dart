import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PhoneHintTextField extends StatefulWidget {
  final bool autoFocus;
  final FocusNode? focusNode;
  final ValueChanged<String>? onChanged;
  final List<TextInputFormatter>? inputFormatters;
  final FormFieldValidator? validator;
  final InputDecoration? decoration;
  final TextField? child;

  const PhoneHintTextField({
    Key? key,
    this.child,
    this.onChanged,
    this.inputFormatters,
    this.validator,
    this.decoration,
    this.autoFocus = false,
    this.focusNode,
  }) : super(key: key);

  @override
  _PhoneHintTextFieldFieldState createState() =>
      _PhoneHintTextFieldFieldState();
}

class _PhoneHintTextFieldFieldState extends State<PhoneHintTextField> {
  final _textController = TextEditingController();
  static const MethodChannel _channel = MethodChannel('sms_autofill');

  @override
  void initState() {
    enablePhoneHints();
    super.initState();
  }

  enablePhoneHints() async {
    final String? hint = await _channel.invokeMethod('requestPhoneHint');
    if (hint != null) {
      _textController.text = hint;
      _textController.selection = TextSelection.fromPosition(
          TextPosition(offset: _textController.text.length));
    }
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      autofocus: widget.autoFocus,
      focusNode: widget.focusNode,
      inputFormatters: widget.inputFormatters,
      decoration: widget.decoration,
      controller: _textController,
      keyboardType: TextInputType.phone,
      onChanged: widget.onChanged,
    );
  }
}
