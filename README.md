
# Phone Hint Text Field

Phone Hint Text Field package is suggest the list of mobile numbers from your device to your Flutter app.

## Installation 

1. Add the latest version of package to your pubspec.yaml (and run`dart pub get`):
```yaml
dependencies:
  phone_hint_text_field: ^0.0.1
```
2. Import the package and use it in your Flutter App.
```dart
import 'package:phone_hint_text_field/phone_hint_text_field.dart';
```

<hr>

<table>
<tr>
<td>

```dart
class PhoneAuthPage extends StatelessWidget {  
  const PhoneAuthPage({Key? key}) : super(key: key);  
  
  @override  
  Widget build(BuildContext context) {  
    return Scaffold(  
      body: Center(  
        child: PhoneHintTextField(  
          child: TextField()
        ),  
      ),  
    );  
  }  
}
```

</td>
<td>
<img  src="/uploads/18015cfb584d7b6846a0bb6f89ff594b/test.png"  alt="">
</td>
</tr>
</table>
